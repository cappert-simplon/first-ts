// Dans le typescript, récupérer les deux éléments (le btn et la div) puis rajouter un event au click 
// sur le button (vous pouvez mettre juste un console log dedans pour commencer pour voir qu'ça marche)
// Dans cet event, faire en sorte de créer un élément de type 'p' avec un document.createElement,
// et assigner comme textContent 'coucou' à cet élément avant de le append sur la div target 

const target = document.querySelector("#target");
const button = document.querySelector("#creator");
const input = document.querySelector<HTMLInputElement>('#input')
const data: string[] = ['toto', 'tata', 'tutu', 'tété le meilleur joueur du monde']

// first draw on page load
draw();

button?.addEventListener('click', () => {
    console.log('bouton cliqué');
    console.log(input?.value);

    if (input?.value) {
        data.push(input?.value);
    }

    draw();

});


function draw() {
    
    target.innerHTML = '';


    for (const element of data) {
        const para = document.createElement('p');
        para.textContent = element;
        target?.appendChild(para);
    }
}
