/* The Moving square
Créer des nouveaux fichier square.html/square.ts et lier les deux fichiers
Dans le square.html, 
créer une div avec un id playground et dans cet élément mettre une div avec un id square et 
faire en sorte en css que le square soit un carré rouge en position absolute par exemple, 
et que la playground prenne l'intégralité de la page (avec des width: 100vw et height: 100vh)

Dans le typescript, capturer les éléments square et playground et les mettre dans des variables
Rajouter un event au click sur le playground, mais cette fois ci, dans les parenthèses de la fonction, 
on va mettre un paramètre event qui va nous permettre de récupérer la position cliquée par la souris 
(avec les propriétés clientX et clientY de l'event), pas hésiter à les console log pour voir les valeurs.
Assigner la valeur du clientX et du clientY au style.top et au style.left en concatenant un 'px' derrière.

Pourquoi pas rajouter une petite transition sur le square pour faire qu'il se déplace élégamment vers sa destination 
Bonus: Faire en sorte que le carré suive la souris sans avoir à cliquer
Autre bonus, faire que le carré se positionne avec la souris au centre du click/déplacement plutôt qu'au coin supérieur gauche 

Encore d'autres bonus : Faire que tant qu'on maintien le bouton de la souris cliqué sur le carré, ça augmente sa taille progressivement (et si on fait pareil avec en plus la touche ctrl enfoncée, ça réduit sa taille)
*/


const playground = document.getElementById("playground");
const square = document.getElementById("square");



