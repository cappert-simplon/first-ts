// créer une variable list qui contiendra un array de task (Task[]) et l'initialiser en tableau vide

let taskList: Task[] = [];

// Créer une fonction addTask qui va attendre en argument un label de type string 
// et qui va pusher un objet task dans la list, avec son done à false

function addTask(label: string) {
    taskList.push({ label: label, done: false });
}


// Créer une fonction toggleDone(index:number) qui va récupérer la valeur à l'index donné 
// dans la list et qui va passer la valeur de done à true si elle est false et inversement
function toggleDone(index: number) {
    taskList[index].done = !(taskList[index].done);
}

// Faire une fonction displayList qui va faire une boucle sur la list et afficher avec un console log 
// chaque task avec si elle est terminée ou pas 

function displayList() {
    let output = "-------------------------------";

    output += "taille: " + taskList.length;

    if (taskList.length == 0) {
        output += "liste de tâches vide.";
    }
    output += "===============================";
    for (let index = 0; index < taskList.length; index++) {
        const donePrint:string = taskList[index].done ? "[x] " : "[ ] ";

        output += (donePrint + taskList[index].label);
    }
    // missing all the \n :(
    console.log(output);
}

// Faire un while qui va utiliser un prompt pour récupérer une valeur et la donner à manger à la fonction addTask
// Exemple de récupération d'input: const input = prompt('question ?');

while (true) {
    let input = prompt("ajouter une task?");

    // cheat code to break the while
    if(input == "42") {
        break;
    }

    if (null != input) {
        addTask(input);
    }
    displayList();
}
