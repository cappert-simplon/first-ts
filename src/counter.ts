/* Faire un petit compteur en TS
Soit dans le fichier event ou dans un fichier counter.html/counter.ts, mettre dans le HTML 2 button, 
un avec un + dedans et l'autre avec un - et mettre également un élément de type p ou span ou peu importe qui contiendra 0 pour le moment

Dans le fichier ts, créer une variable counter qui sera initialisée à 0, 
puis faire un querySelector pour récupérer le button avec le + dedans 
(vous pouvez lui rajouter un id, une classe ou ce que vous voulez pour le sélectionner plus facilement)
Sur cet élément, rajouter un Event Listener au click qui incrémentera la valeur de counter de 1 
et qui fera un console log de sa valeur actuelle
Une fois que ça marche, faire en sorte de capturer l'élément p/span/autre et modifier son texte pour 
lui mettre la valeur de counter (toujours à l'intérieur du event listener)
Faire pareil pour le button - qui décrémentera le counter de 1 au click
Bonus : On peut rajouter un bouton retour à zéro. 
Autre bonus, faire ue le increment et le decrement soit une seule et même fonction appelé par les deux event 
*/

let counter = 0;

const plus = document.querySelector<HTMLElement>("#plus");
const minus = document.querySelector<HTMLElement>("#minus");

const resultEt = document.querySelector<HTMLElement>("#result");

plus?.addEventListener("click", processAndShow)
minus?.addEventListener("click", processAndShow)

function processAndShow(this:HTMLElement) {

    if (this.id == "plus") {
        counter++;
    } else {
        counter--;
    }
    console.log(counter);

    if (resultEt) {
        resultEt.textContent = counter.toString();
    }
}



