// créer et exporter un type Task
// propriété label de type string et une propriété done en boolean
interface Task {
    label:string;
    done:boolean;
}