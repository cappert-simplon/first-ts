# exos typescript
## Exo Guess a number
Dans le main.ts, faire un guess a number
Exemple de récupération d'input utilisateurice : const input = prompt('question ?');
1. Créer une variable toGuess qui contiendra le nombre à deviner, pour l'instant on peut mettre un nombre en dur
2. Ensuite on peut créer une autre variable isGame booléenne qui contient true par défaut
3. Faire une boucle while qui va tourner tant que isGame
4. Dans la boucle, faire un prompt pour récupérer un input utilisateur (ici, qu'est-ce qu'on pense qu'est le nombre à deviner)
5. Ensuite, on fait une condition pour vérifier si ce qui a été proposé correspond à la variable toGuess, si oui, on passe le isGame à false et on met un ptit console log de bravo, si non on met juste un ptit message que c'est pas ça
⚠️ Il va falloir convertir l'input en number, pour ça on peut utiliser google ou bien : Number(prompt('question'));
Bonus : Faire en sorte que plutôt que juste gagné perdu, ça nous dise si c'est plus ou si c'est moins, et aussi que ça nous engueule si jamais l'input n'est pas un number 


## Exo Todo List rapide en TS
1. Créer un fichier todolist.html et créer un fichier src/todolist.ts, et charger ce fichier dans le html (comme c'est fait dans le index.html pour le main.ts)
2. Dans le fichier src/entities.ts (ou un autre fichier, peu importe), créer et exporter un type Task qui, comme on l'avait fait en java, va posséder une propriété label de type string et une propriété done en boolean
3. Dans le fichier todolist.ts, créer une variable list qui contiendra un array de task (Task[]) et l'initialiser en tableau vide
4. Créer une fonction addTask qui va attendre en argument un label de type string et qui va pusher un objet task dans la list, avec son done à false
5. Créer une fonction toggleDone(index:number) qui va récupérer la valeur à l'index donné dans la list et qui va passer la valeur de done à true si elle est false et inversement
6. Faire un while qui va utiliser un prompt pour récupérer une valeur et la donner à manger à la fonction addTask
7. Faire une fonction displayList qui va faire une boucle sur la list et afficher avec un console log chaque task avec si elle est terminée ou pas 

## Exercice manipulation DOM
1. Créer un fichier dom.html et un fichier src/dom.ts et les lier ensemble
2. Mettre ceci dans le HTML : 
<section id="section1">
        <h2>Section 1 Title</h2>
        <p>Some text in the first section <span class="colorful">with text in span</span> coucou</p>
        <a href="#">See more</a>
    </section>

    <section id="section2">
        <h2 class="colorful">Section 2 title</h2>
        <p id="para2">Some text in the second section</p>
    </section>

3. Et voilà une liste de truc à faire via le typescript en utilisant des querySelector :
* Mettre le texte de l'élément avec l'id para2 en bleu
* Mettre une border en pointillet noire à la section2
* Mettre une background color orange à l'élément de la classe colorful de la section2
* Mettre le h2 de la section1 en italique
* Cacher l'élément colorful situé dans un paragraphe
* Changer le texte de para2 pour "modified by JS"
* Changer le href du lien de la section1 pour le faire aller sur simplonlyon.fr
* Rajouter la classe big-text sur le h2 de la section2
* Bonus : Faire que tous les paragraphe du document soit en italique 
Exemple de sélection d'un élément : const divApp = document.querySelector<HTMLElement>('#app') où #app peut être n'importe quel sélecteur css possible
JimCOT — Aujourd’hui à 10:52
Une autre technique pour récupérer l'élément enfant d'un parent : 
 const colorful = section2.querySelector<HTMLElement>('.colorful'); 

## révision création d'élements en typescript
### I. Bouton qui rajoute un paragraphe
1. Créer un fichier creation.html et src/creation.ts et les lier ensemble
2. Dans le creation.html, on rajoute une div avec un id target et un button avec un id "creator"
3. Dans le typescript, récupérer les deux éléments (le btn et la div) puis rajouter un event au click sur le button (vous pouvez mettre juste un console log dedans pour commencer pour voir qu'ça marche)
4. Dans cet event, faire en sorte de créer un élément de type 'p' avec un document.createElement, et assigner comme textContent 'coucou' à cet élément avant de le append sur la div target 

### II. Contenu du para qui vient d'un input
1. Dans le fichier html, rajouter un input, pourquoi pas avec un id aussi et le capturer dans le typescript
2. Modifier l'event au click pour faire qu'au lieu de mettre 'coucou' en textContent du nouveau p, on mette plutôt la valeur actuelle de l'input (aide : on peut récupérer la valeur d'un input avec input.value, si on a effectivement appelé la variable input)
### III. Penser data
1. Rajouter un array de string avec quelques valeurs par défaut dans le typescript
2. Créer une fonction draw() qui va faire une boucle sur cet array, et à chaque tour de boucle, elle va faire presqu'exactement ce qu'on faisait jusqu'à maintenant dans le event au click (createElement, textContent, append), mais cette fois en mettant comme textContent la valeur actuelle de la boucle plutôt que la valeur de l'input
3. Vider tout ce qu'il y a dans l'eventListener du button, et à la place, faire qu'on rajoute la valeur actuelle de l'input dans le tableau (aide:  avec un tableau.push()) avant de déclencer la méthode draw()
4. Faire en sorte de vider le HTML du target au début du draw en lui mettant son innerHTML vide 
